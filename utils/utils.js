// mque.xcooline.com
// const APIURL = 'https://oa.xstxhr.com/api/';
// const UPImageUrl= 'https://oa.xstxhr.com'
const APIURL = 'http://mque.xcooline.com/api/'
const UPImageUrl = 'http://mque.xcooline.com/api/file_img' // 图片上传
const UPVideoUrl = 'http://mque.xcooline.com/api/file_video' // 视屏上传
const JGAppkey ='180ea3370ee8d7292e215769' // 极光Appkey
const JGMasterSecret='98cdfd7283c3805453380380' // Master Secret 
const JGIMREST = 'https://report.im.jpush.cn/v2' // IM REST Report V2地址
// 封装post请求
function HttpPost(url, data, result) {
	const accessToken = uni.getStorageSync('accessToken');
	uni.request({
		url: APIURL + url,
		data: data,
		header: {
			// "Content-Type": 'multipart/form-data',
			"accessToken": accessToken
		},
		method: "POST",
		success: function(res) {
			if (res.data.code == 9000) {
				uni.hideLoading();
				uni.showModal({
					title: '登陆已过期,请重新登录',
					showCancel: false,
					success: function(res) {
						uni.navigateTo({
							url: '/pages/login/login'
						})
					}
				})
			} else if (res.data.code == 0) {
				uni.hideLoading();
				uni.showToast({
					title: res.data.msg,
					icon: 'none'
				})
			} else {
				return typeof result == 'function' && result(res.data)
			}
		},
		fail: function(res) {
			uni.showModal({
				title: res,
				showCancel: false
			})
		}
	})
}

// 封装Get请求
function HttpGet(url, data = '', result) {
	const accessToken = uni.getStorageSync('accessToken');
	uni.request({
		url: APIURL + url,
		data: data,
		header: {
			"accessToken": accessToken,
		},
		method: "GET",
		success: function(res) {
			if (res.data.code === 200) {
				return typeof result == 'function' && result(res.data)
			} else {
				uni.showModal({
					title: res.data.message,
					showCancel: false
				})
			}

		}
	})
}

// 提示
function tips(text) {
	uni.showToast({
		title: text,
		icon: 'none'
	})
}

// 返回上一层
function back() {
	uni.navigateBack({
		delta: 1
	})
}

// 将毫秒转成年月日
function MillisecondToDate(msd) {
	const date = new Date(msd)
	var Y = date.getFullYear() + '年'
	var M = (date.getMonth() < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1) + '月'
	var D = (date.getDate() < 10 ? '0' + (date.getDate() + 0) : date.getDate() + 0) + ''
	var h = date.getHours() + ':'
	var m = date.getMinutes() + ':'
	var s = (date.getSeconds() < 10 ? '0' + (date.getSeconds() + 0) : date.getSeconds() + 0)
	// return Y + M + D + '\xa0\xa0' + h + m + s
	return M + D + '\xa0' + h + m + s
}

// 获取当前系统时间
function ToDate() {
	var date = new Date();
	var Y  = date.getFullYear();
	var M = (date.getMonth() < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1);
	var D = (date.getDate() < 10 ? '0' + (date.getDate() + 0) : date.getDate() + 0);
	var h = (date.getHours() < 10 ? '0' + (date.getHours() + 0) : date.getHours() + 0);
	var m = (date.getMinutes() < 10 ? '0' + (date.getMinutes() + 0) : date.getMinutes() + 0);
	var s = (date.getSeconds() < 10 ? '0' + (date.getSeconds() + 0) : date.getSeconds() + 0);
	return　(Y + '-' + M + '-' + D  + '%20' + h + ':' + m + ':' + s);
}
// 获取系统时间前多少天
function ToDateStare(data) {
	var now = new Date();
	var date = new Date(now.getTime() - data * 24 * 3600 * 1000);
	var Y  = date.getFullYear();
	var M = (date.getMonth() < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1);
	var D = (date.getDate() < 10 ? '0' + (date.getDate() + 0) : date.getDate() + 0);
	var h = (date.getHours() < 10 ? '0' + (date.getHours() + 0) : date.getHours() + 0);
	var m = (date.getMinutes() < 10 ? '0' + (date.getMinutes() + 0) : date.getMinutes() + 0);
	var s = (date.getSeconds() < 10 ? '0' + (date.getSeconds() + 0) : date.getSeconds() + 0);
	return　(Y + '-' + M + '-' + D  + '%20' + h + ':' + m + ':' + s);
}
// 暴露方法
module.exports = {
	HttpPost: HttpPost,
	HttpGet: HttpGet,
	APIURL: APIURL,
	UPImageUrl: UPImageUrl,
	UPVideoUrl: UPVideoUrl,
	tips: tips,
	back: back,
	JGAppkey: JGAppkey,
	JGIMREST: JGIMREST,
	MillisecondToDate: MillisecondToDate,
	ToDate:　ToDate,
	ToDateStare: ToDateStare,
	JGMasterSecret: JGMasterSecret
}
