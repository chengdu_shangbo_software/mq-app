import Vue from 'vue'
import App from './App'
import uView from "uview-ui"
import JIM from '@/utils/jmessage-wxapplet-sdk-1.4.3.min.js';
// const jv = new JIM() // 创建实例
Vue.prototype.$jv = new JIM()  // 挂载实例 this.$jv调用
Vue.config.productionTip = false
Vue.use(uView)
App.mpType = 'app'

const app = new Vue({
    ...App
})
app.$mount()
